<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class transport extends Model
{
    // protected $connection = 'mongodb';
    protected $collection = 'transport';

    protected $fillable = [
        'id', 'cargo_capacity','consumables','cost_in_credits','created','crew','edited','length','manufacturer','max_atmospering_speed','model','name','passengers'
    ];
}
