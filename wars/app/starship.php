<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class starship extends Model
{
    // protected $connection = 'mongodb';
    protected $collection = 'starships';

    protected $fillable = [
        'id', 'MGLT','hyperdrive_rating','pilots','starship_class'
    ];
}
