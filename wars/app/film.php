<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class film extends Model
{
    // protected $connection = 'mongodb';
    protected $collection = 'films';

    protected $fillable = [
        'id', 'characters','created','director','edited','episode_id','opening_crawl','planets','producer','release_date','species','starships','title','vehicles'
    ];
}
