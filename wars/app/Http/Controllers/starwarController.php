<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\film;
use App\people;
use App\planet;
use App\specie;
use App\starship;
use App\transport;
use App\vehicle;
use DB;


class starwarController extends Controller
{
    public function index(Request $request)
    {
    	$film = film::select('title')->whereRaw('episode_id = (select max(`episode_id`) from films)')->first();

    	$filmsdara = DB::select("select * from 
								(select db.*, dense_rank() OVER (order by films DESC) as rank
								from
								(select people_id,people_nm,count(distinct film_id) as films
								from
								(SELECT a.film_id,a.people_id,b.name as people_nm FROM `films_characters`as a
								inner join peoples as b
								on a.people_id=b.id group by 1,2,3) as data
								group by 1,2)as db
								group by 1,2,3) db1
								where rank=1");

    	$filmspice = DB::table('species')
    				  ->select('name',DB::raw("count(*) as count"))
    				  ->join('films_species','films_species.species_id','=','species.id')
    				  ->groupBy('film_id')
    				  ->get();

    	$flimpiolet = DB::select("select db3.*,db4.species_nm,db4.people_nm  from
								(select * from 
								(select db1.*,dense_rank() OVER (order by pilots DESC) as rank
								 from
								 (select  planet_id,planet_nm,count(DISTINCT people_id) as pilots from
								  (select a.*,b.film_id,c.vehicle_id,d.people_id
								   from (SELECT id as planet_id,name as planet_nm FROM `planets` group by 1,2) as a
								   inner JOIN films_planets as b
								   on a.planet_id=b.planet_id
								   inner join films_vehicles as c
								   on b.film_id=c.film_id
								   inner join vehicles_pilots as d
								   on c.vehicle_id=d.vehicle_id
								   group by 1,2,3,4,5) as db
								  group by 1,2) as db1) as db2
								  where rank=1) as db3
								  
								  left JOIN
								  
								  (select a.planet_id,f.name as species_nm,g.name as people_nm
								   from (SELECT id as planet_id,name as planet_nm FROM `planets` group by 1,2) as a
								   inner JOIN films_planets as b
								   on a.planet_id=b.planet_id
								   inner join films_vehicles as c
								   on b.film_id=c.film_id
								   inner join vehicles_pilots as d
								   on c.vehicle_id=d.vehicle_id
								   inner join species_people as e 
								   on d.people_id=e.species_id
								   inner join species as f
								   on e.species_id=f.id
								   inner join peoples as g
								   on d.people_id=g.id
								   group by 1,2,3) as db4
								   
								 on db3.planet_id = db4.planet_id
								 group by 1,2,3,4,5,6");
    	//dd($flimpiolet);

    	return view('show',compact('film','filmsdara','filmspice','flimpiolet'));
    }
}
