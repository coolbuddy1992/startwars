<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class planet extends Model
{
    // protected $connection = 'mongodb';
    protected $collection = 'planets';


    protected $fillable = [
        'id', 'climate','created','diameter','edited','gravity','name','orbital_period','population','rotation_period','surface_water','terrain'
    ];
}
