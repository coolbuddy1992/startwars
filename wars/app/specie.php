<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class specie extends Model
{
    // protected $connection = 'mongodb';
    protected $collection = 'species';

    protected $fillable = [
        'id', 'average_height','average_lifespan','classification','created','designation','edited','eye_colors','hair_colors','homeworld','language','name','people','skin_colors'
    ];
}
