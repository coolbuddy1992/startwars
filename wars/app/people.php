<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class people extends Model
{
    // protected $connection = 'mongodb';
    protected $collection = 'people';

    protected $fillable = [
        'id', 'birth_year','created','edited','eye_color','gender','hair_color','height','homeworld','mass','name','skin_color'
    ];
}
