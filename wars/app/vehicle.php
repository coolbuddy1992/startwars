<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class vehicle extends Model
{
    // protected $connection = 'mongodb';
    protected $collection = 'vehicles';

    protected $fillable = [
        'id', 'pilots','vehicle_class'
    ];
}
}
