<div class="container">
	<div class="form-group">
		<h2>Which of all StarWarsmovies hasthe longest opening crawl(counted by number
of characters)?</h2>
		<p class="aligndata">{{$film->title}}</p>
	</div>
	<div class="form-group">
		<h2>What character (person) appeared in most of the Star Wars films?</h2>
		@foreach($filmsdara as $filmsdaras)
		<p class="aligndata">{{$filmsdaras->people_nm}}</p>
		@endforeach
	</div>
	<div class="form-group">
		<h2>What species (i.e. characters that belong to certain species) appeared in the most
number of Star Warsfilms?</h2>
		@foreach($filmspice as $filmspices)
			<p class="aligndata"><span>{{$filmspices->name}}({{$filmspices->count}})</p>
		@endforeach
	</div>
	<div class="form-group">
		<h2>What planet in Star Wars universe provided largest number of vehicle pilots?</h2>
		@foreach($flimpiolet as $flimpiolets)
		<p class="aligndata">Planet:{{$flimpiolets->planet_nm}}-Pilots:({{$flimpiolets->pilots}}){{$flimpiolets->people_nm}},{{$flimpiolets->species_nm}}</p>
		@endforeach
	</div>
</div>