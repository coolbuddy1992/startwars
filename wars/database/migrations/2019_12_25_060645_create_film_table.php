<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //film
        Schema::create('films', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('created')->nullable();
            $table->string('director')->nullable();
            $table->dateTime('edited')->nullable();
            $table->string('episode_id')->nullable();
            $table->string('opening_crawl')->nullable();
            $table->string('producer')->nullable();
            $table->dateTime('release_date')->nullable();
            $table->string('title')->nullable();
            $table->timestamps();
        });

        //people
        Schema::create('peoples', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('birth_year')->nullable();
            $table->dateTime('created')->nullable();
            $table->dateTime('edited')->nullable();
            $table->string('eye_color')->nullable();
            $table->string('gender')->nullable();
            $table->string('hair_color')->nullable();
            $table->string('height')->nullable();
            $table->string('homeworld')->nullable();
            $table->string('mass')->nullable();
            $table->string('name')->nullable();
            $table->string('skin_color')->nullable();
            $table->timestamps();
        });

        //planets
        Schema::create('planets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('climate')->nullable();
            $table->dateTime('created')->nullable();
            $table->string('diameter')->nullable();
            $table->dateTime('edited')->nullable();
            $table->string('gravity')->nullable();
            $table->string('name')->nullable();
            $table->string('orbital_period')->nullable();
            $table->string('population')->nullable();
            $table->string('rotation_period')->nullable();
            $table->string('surface_water')->nullable();
            $table->string('terrain')->nullable();
            $table->timestamps();
        });
        //species
        Schema::create('species', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('average_height')->nullable();
            $table->string('average_lifespan')->nullable();
            $table->string('classification')->nullable();
            $table->dateTime('created')->nullable();
            $table->string('designation')->nullable();
            $table->dateTime('edited')->nullable();
            $table->string('eye_colors')->nullable();
            $table->string('hair_colors')->nullable();
            $table->string('homeworld')->nullable();
            $table->string('language')->nullable();
            $table->string('name')->nullable();
            $table->string('skin_colors')->nullable();
            $table->timestamps();
        });
        //transports
        Schema::create('transports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cargo_capacity')->nullable();
            $table->string('consumables')->nullable();
            $table->string('cost_in_credits')->nullable();
            $table->dateTime('created')->nullable();
            $table->string('crew')->nullable();
            $table->dateTime('edited')->nullable();
            $table->string('length')->nullable();
            $table->string('manufacturer')->nullable();
            $table->string('max_atmosphering_speed')->nullable();
            $table->string('model')->nullable();
            $table->string('name')->nullable();
            $table->string('passengers')->nullable();
            $table->timestamps();
        });
        //starships
        Schema::create('starships', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('MGLT')->nullable();
            $table->string('hyperdrive_rating')->nullable();
            $table->string('starship_class')->nullable();
            $table->timestamps();
        });
        //vehicles
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vehicle_class')->nullable();
            $table->timestamps();
        });
        //films_characters
        Schema::create('films_characters', function (Blueprint $table) {
            $table->integer('film_id');
            $table->integer('people_id');
            $table->timestamps();
        });
        //films_planets
        Schema::create('films_planets', function (Blueprint $table) {
            $table->integer('film_id');
            $table->integer('people_id');
            $table->timestamps();
        });
        //films_species
        Schema::create('films_species', function (Blueprint $table) {
            $table->integer('film_id');
            $table->integer('species_id');
            $table->timestamps();
        });
        //films_starships
        Schema::create('films_starships', function (Blueprint $table) {
            $table->integer('film_id');
            $table->integer('starship_id');
            $table->timestamps();
        });
        //films_vehicles
        Schema::create('films_vehicles', function (Blueprint $table) {
            $table->integer('film_id');
            $table->integer('vehicle_id');
            $table->timestamps();
        });
        //species_people
        Schema::create('species_people', function (Blueprint $table) {
            $table->integer('species_id');
            $table->integer('people_id');
            $table->timestamps();
        });
        //starships_pilots
        Schema::create('starships_pilots', function (Blueprint $table) {
            $table->integer('starship_id');
            $table->integer('people_id');
            $table->timestamps();
        });
        //vehicles_pilots
        Schema::create('vehicles_pilots', function (Blueprint $table) {
            $table->integer('vehicle_id');
            $table->integer('people_id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
        Schema::dropIfExists('peoples');
        Schema::dropIfExists('planets');
        Schema::dropIfExists('species');
        Schema::dropIfExists('transports');
        Schema::dropIfExists('starships');
        Schema::dropIfExists('vehicles');
        Schema::dropIfExists('films_characters');
        Schema::dropIfExists('films_planets');
        Schema::dropIfExists('films_species');
        Schema::dropIfExists('films_starships');
        Schema::dropIfExists('films_vehicles');
        Schema::dropIfExists('species_people');
        Schema::dropIfExists('starships_pilots');
        Schema::dropIfExists('vehicles_pilots');
    }
}
